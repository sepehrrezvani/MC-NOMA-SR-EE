%% Monto-Carlo Simulation: Channel Realizations
clear all
clc
BS_Type=1; %Macro-BS: (BS_Type=1) / Femto-BS: (BS_Type=0).
radius_BS=BS_Type*500+(1-BS_Type)*40; %radius of MBS/FBS in meter
K_vector=[5:5:60]; %number of users
min_Dis_User=BS_Type*20+(1-BS_Type)*2; %minimum distance of user to associated BS in meter
PSD_N0=-174; %PSD of Noise in dBm
PSD_N0=10.^((PSD_N0-30)/10); %PSD of Noise in watts
channel_sample=3; %number of channel realizations
fprintf('***Total Samples***\n\n')
disp(length(K_vector)*channel_sample)
for index_Kvec=1:numel(K_vector)
    for sample=1:channel_sample
        %% Simulation Settings
        %%%%%Network Topology
        PSD_users=[]; Cor_user=[]; h=[];
        K=K_vector(index_Kvec);
        PSD_users=abs(randn(1,K)).*PSD_N0; %PSD of AWGN at users
        PSDnoise_samples{index_Kvec,sample}=PSD_users; %save PSD of AWGN
        Cor_user=func_CoordinateUser(K,radius_BS,min_Dis_User); %Coordinate of users (uniform distribution)
        h=func_SingleChannelGain(K,Cor_user,BS_Type); %(flat fading) channel gains
        h_sample{index_Kvec,sample}=h; %save channel gain h
    end
end
save('STEP1_Settings','BS_Type','radius_BS','K_vector','min_Dis_User','h_sample','PSDnoise_samples','channel_sample')


