function [q,p,r,F,lambda_matrix,S,nu]=func_EEmax_Dinkelbach_Subgradient(K,N,W_s,R_min,h,activeuser,...
        Modify_order,cluster_head,P_max,P_mask,Q_min,eps_dinkel,P_C,cancel,eps_subgrad)
lambda=10; %Initialization: Finding lambda such that F(lambda,p)>=0
[q,alpha,c]=func_SumRateMax_q(K,N,W_s,R_min,h,activeuser,...
        Modify_order,cluster_head,P_max,P_mask,Q_min); %Intra-cluster power allocation
[p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
[r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
q_sumrate=q; p_sumrate=p; r_sumrate=r; %%Save
while (sum(r) - lambda*(sum(q)+P_C) <= 0)
    lambda=lambda/2; %Reduce lambda until f(lambda_0,p)>=0
end
%%Dinkelbach Algorithm
%H_n
for n=1:N
    H(n)=alpha(n)*h(cluster_head(n),n);
end
%Determine Q_min_tilde P_mask_tilde P_max_tilde
Q_min_tilde=Q_min-c./alpha;
P_mask_tilde=P_mask-c./alpha;
P_max_tilde=P_max-sum(c./alpha);
iter_Dinkel=1; %Dinkelbach iteration index
F=0;
F(iter_Dinkel)=(sum(r) - lambda*(sum(q)+P_C));
S=0; %first row of subgradient matrix is zero
lambda_matrix=0;
lambda_matrix(1,iter_Dinkel)=lambda; %saving matrix of lambda over iterations
while F(iter_Dinkel)>eps_dinkel
    iter_Dinkel=iter_Dinkel+1; %update Dinkelbach iteration index
    %%%%%Update lambda
    [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
    [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
    lambda=sum(r)/(sum(q)+P_C);
    lambda_matrix(1,iter_Dinkel)=lambda;
    %%%%%Find optimal q - Solve via Subgradient method
%         [q,p,r,S,nu]=func_EEmax_InnerSubgradient(K,N,W_s,R_min,h,activeuser,...
%         Modify_order,P_max_tilde,P_mask_tilde,Q_min_tilde,P_C,cancel,iter_Dinkel,lambda,S,H,c,alpha,r,eps_subgrad);
        %Lagrange dual with subgradient method
        nu=1; %initial Lagrange multiplier
        for n=1:N
            q_tilde(n) = max( Q_min_tilde(n) , min((1/log(2))/(nu+lambda)-1./H(n),P_mask_tilde(n)) );
        end
        iter_subg=1; %Subgradient iteration index at each Dinkelbach iteration
        %Calculate F(lambda,p) at each subgradient iteration
        q=q_tilde+c./alpha;
        S_old=sum(r) - lambda*(sum(q)+P_C);
        S(iter_Dinkel,iter_subg)=S_old;
        Diff=abs(S_old); %stopping criteria for subgradient method
        while (Diff>eps_subgrad)
            S_old=S(iter_Dinkel,iter_subg); %save prior S_old for stopping condition
            iter_subg=iter_subg+1; %update iteration index of subgradient method
            %update nu
            eps_s=min(nu/2^(iter_subg),1e-3);
            nu=max( nu - eps_s*(P_max_tilde - sum(q_tilde)) ,0);
            for n=1:N
                q_tilde(n) = max( Q_min_tilde(n) , min((1/log(2))/(nu+lambda)-1./H(n),P_mask_tilde(n)) );
            end
            %update q
            q=q_tilde+c./alpha;
            %update S
            [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
            [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
            S(iter_Dinkel,iter_subg)=(sum(r) - lambda*(sum(q)+P_C));
            %Diff func
            Diff=abs(S(iter_Dinkel,iter_subg) - S_old);
        end
    %Calculate F
    F(iter_Dinkel)=(sum(r) - lambda*(sum(q)+P_C));
end