function [rho]=func_NOMA_Clustering(K,N,Decod_order)
rho=zeros(K,N);
n=0;
user_rank=Decod_order(:,1); %In TD-NOMA, the decoding order of users over different resource blocks are the same, so choose one
while sum(user_rank)>0
    bestuser=find(user_rank==max(user_rank));
    n=n+1; if (n>N) n=1; end
    rho(bestuser,n)=1;
    user_rank(bestuser)=0;
end