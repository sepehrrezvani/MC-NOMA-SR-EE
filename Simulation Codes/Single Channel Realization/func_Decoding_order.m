function [Decod_order]=func_SICordering(K,N,h)
Decod_order=zeros(K,N);
for n=1:N
    count=0;
    while(count <= K-1)
        stronguser=find(h(:,n)==max(h(:,n)));
        Decod_order(stronguser,n)=K-count;
        count=count+1;
        h(stronguser,n)=0;
    end
end
