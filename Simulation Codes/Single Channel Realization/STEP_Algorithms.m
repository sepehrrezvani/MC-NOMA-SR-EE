clear all
clc
%% Network Topology and General System Settings
BS_Type=1; %Macro-BS: (BS_Type=1) / Femto-BS: (BS_Type=0).
radius_BS=BS_Type*500+(1-BS_Type)*40; %radius of MBS/FBS in meter
K=15; %Number of users
U_NOMA=4; %Maximum number of multiplexed users in NOMA
min_Dis_User=BS_Type*20+(1-BS_Type)*2; %minimum distance of user to associated BS in meter
W=5e+6; %wireless bandwidth (Hz)
PSD_N0=-174; %PSD of Noise in dBm
PSD_N0=10.^((PSD_N0-30)/10); %PSD of Noise in watts
PSD_users=abs(randn(1,K)).*PSD_N0; %PSD of AWGN at users
Cor_user=func_CoordinateUser(K,radius_BS,min_Dis_User); %Coordinate of users (uniform distribution)
h=func_SingleChannelGain(K,Cor_user,BS_Type); %(flat fading) channel gains
h_SC=h;%save (single) channels
%%%%%Transmit power of BSs
P_max=BS_Type*46+(1-BS_Type)*30; %BS power in dbm
P_max=10.^((P_max-30)./10); %BS power in Watts
%%%%%Minimum rate demands
% R_min=2e+6.*abs(randn(1,K)); %bps
R_min=1.5e+6*ones(1,K); %bps
%Circuit power consumption in Watts>>> Change it later
P_C=BS_Type*1+(1-BS_Type)*1; %(W)

%Threshold parameters for algorithms
eps_dinkel=1e-8; %stopping criterion for Dinkelbach algorithm
eps_subgrad=1e-6; %stopping criterion for subgradient method
%barrier method initialization
ALPHA=0.1; %alpha in backtracking line search (0<alpha<0.5)
BETA=0.8; %beta in backtracking line search (0<beta<1)
eps_N=1e-3; %accuracy parameter for stopping criterion Newton's (inner) iteration
epsilon_B=1e-3; %accuracy parameter for stopping criterion of Barrier (outer) iteration
stepsize=10; %step size for accuracy parameter in the range 10 � 100
tt=2; %initializing the accuracy parameter of Barrier iteration
Max_Newton=20; %maximum number of Newton's (inner) iteration

%% Figure of Network Topology: Save as 'Fig_topology.fig'
[Leg_coverage_BS]=func_FigTopology(K,radius_BS,Cor_user);
% clear Dis_BS Cor_BS radius_BS min_Dis_User Cor_user Leg_coverage_BS b F_cell ...
%   M_cell R_macro R_femto

%% SC-NOMA: Multiplexing all users over a single flat fading channel
fprintf('*********SC-NOMA: Benchmark*********\n\n\n')
U_max=K; %all users are multiplexed
N=ceil(K/U_max); %number of clusters (here is 1), or number of resource blocks
W_s=W/N; %bandwidth of each subchannel
N0=W_s*repmat(PSD_users',1,N); %AWGN power at users over the single channel
h=repmat(h_SC',1,N); %channel gains
h=h./N0; %channel gains normalized by noise
P_mask=P_max*ones(1,N); %No additional limit on the maximum power of the single channel

%Determine user grouping and active users on each resource block (group)
rho=ones(K,N); 
for n=1:N
    activeuser{n}=find(rho(:,n)); %Set of active users on each group (or time block)
end

%%Decoding Order
[Decod_order]=func_Decoding_order(K,N,h); %lower number corresponds to lower decoding order
[cancel]=func_Cancellation(K,N,Decod_order); %'Cancel(m,m_prn,n)=1' means that user 'm_prn' can cancel the signal of user 'm' on resource block 'n'
[Modify_order]=func_Decoding_order_Modify(K,N,rho,Decod_order); %Modify ordering numbers from 1 to oder of cluster, and set zero for users do not belong to the cluster set
for n=1:N
    cluster_head(n)=find(Modify_order(:,n)==max(Modify_order(:,n))); %Vector of Cluster-head users index
end

%%Power Minimization Problem (Feasibility problem)
[Q_min,p]=func_PowMin(K,N,W_s,R_min,h,Modify_order,activeuser); %Find lower bound Q^{min}_n as well as optimal powers
P_powmin_FSCSIC=sum(Q_min); % Minimal total power consumption to satisfy minimum rate demands (in Watts)
[r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
%%Feasibility Check
fprintf('***Power minimization problem***\n\n\n')
%%%%%Results
fprintf('Minimum rate demand of users (Mbps):\n')
disp(R_min/1e+6)
fprintf('Achievable rate of users (Mbps):\n')
rate_FSCSIC=W_s*r/1e+6;
disp(rate_FSCSIC)
fprintf('Total power consumption (mW):\n')
disp(P_powmin_FSCSIC*1e+3)
fprintf('Total power consumption in percentage of maximum cellular power:\n')
disp(100*P_powmin_FSCSIC/P_max)
if ( P_powmin_FSCSIC>P_max )
    Feasible_FSCSIC=0;
    fprintf('---Infeasible! (Not Enough Power!)---\n\n')
else Feasible_FSCSIC=1;
    fprintf('---Feasible!---\n\n')
end

%%Sum-Rate Maximization Problem
if Feasible_FSCSIC==1
    [q,alpha,c]=func_SumRateMax_q(K,N,W_s,R_min,h,activeuser,...
        Modify_order,cluster_head,P_max,P_mask,Q_min); %Intra-cluster power allocation
    [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
    [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
    fprintf('***Sum-rate maximization problem***\n\n\n')
    %%%%%Results
    fprintf('Minimum rate demand of users (Mbps):\n')
    disp(R_min/1e+6)
    fprintf('Achievable rate of users (Mbps):\n')
    rate_FSCSIC=W_s*r/1e+6;
    disp(rate_FSCSIC)
    fprintf('Cluster-head user`s index:\n')
    disp(cluster_head)
    fprintf('Sum-rate of users (Mbps):\n')
    R_tot_FSCSIC=sum(rate_FSCSIC);
    disp(R_tot_FSCSIC)
    fprintf('Total power consumption in percentage of maximum cellular power:\n')
    disp(100*sum(q)/P_max)
end

%%Energy Efficiency Maximization Problem
if Feasible_FSCSIC==1
    %Dinkelbach Algorithm - Lagrange dual with subgradient method
    [q,p,r,F,lambda_matrix,S,nu]=func_EEmax_Dinkelbach_Subgradient(K,N,W_s,R_min,h,activeuser,...
        Modify_order,cluster_head,P_max,P_mask,Q_min,eps_dinkel,P_C,cancel,eps_subgrad);
    fprintf('***Energy efficiency maximization problem: Subgradient Method***\n\n\n')
    %%%%%Results
    fprintf('Achievable rate of users (Mbps):\n')
    rate_FSCSIC=W_s*r/1e+6;
    disp(rate_FSCSIC)
    fprintf('Sum-rate of users (Mbps):\n')
    disp(sum(rate_FSCSIC))
    fprintf('System energy efficiency (Mbits/Joule):\n')
    EE_FSCSIC=sum(rate_FSCSIC)/(sum(q)+P_C);
    disp(EE_FSCSIC)
    fprintf('Total power consumption in percentage of maximum cellular power:\n')
    disp(100*sum(q)/P_max)
    fprintf('Dinkelbach parameter lambda over iterations\n')
    disp(lambda_matrix)
    fprintf('Fractional function over Dinkelbach iterations: F(lambda,p)\n')
    disp(F)
    fprintf('F(lambda,p) over subgradient iterations at each Dinkelbach iteration\n')
    disp(S)
    
    %Dinkelbach Algorithm - Barrier algorithm with inner Newton's method
    [q,p,r,F,lambda_matrix,barrier_func]=func_EEmax_Dinkelbach_Barrier(K,N,W_s,R_min,h,activeuser,...
        Modify_order,cluster_head,P_max,P_mask,Q_min,eps_dinkel,P_C,cancel,...
        ALPHA,BETA,eps_N,epsilon_B,stepsize,tt,Max_Newton);
    fprintf('***Energy efficiency maximization problem: Barrier Method***\n\n\n')
    %%%%%Results
    fprintf('Achievable rate of users (Mbps):\n')
    rate_FSCSIC=W_s*r/1e+6;
    disp(rate_FSCSIC)
    fprintf('Sum-rate of users (Mbps):\n')
    disp(sum(rate_FSCSIC))
    fprintf('System energy efficiency (Mbits/Joule):\n')
    disp(sum(rate_FSCSIC)/(sum(q)+P_C))
    fprintf('Total power consumption in percentage of maximum cellular power:\n')
    disp(100*sum(q)/P_max)
    fprintf('Dinkelbach parameter lambda over iterations\n')
    disp(lambda_matrix)
    fprintf('Fractional function over Dinkelbach iterations: F(lambda,p)\n')
    disp(F)
    fprintf('Barrier function in the last Dinkelback iteration\n')
    disp(barrier_func{numel(lambda_matrix)})
end

%% FDMA-NOMA (FD-NOMA): User grouping over subchannels (flat fading channels)
fprintf('*********FDMA-NOMA (FD-NOMA)*********\n\n\n')
U_max=U_NOMA; %maximum number of multiplexed users
N=ceil(K/U_max); %number of subchannels (clusters)
W_s=W/N; %Bandwidth of each subchannel
N0=W_s*repmat(PSD_users',1,N); %AWGN power at users over subchannels
h=repmat(h_SC',1,N); %channel gains
h=h./N0; %channel gains normalized by noise
P_mask=P_max*ones(1,N); %No additional limit on the maximum power of the subchannels

%%Decoding Order
[Decod_order]=func_Decoding_order(K,N,h); %lower number corresponds to lower decoding order
[cancel]=func_Cancellation(K,N,Decod_order); %'Cancel(m,m_prn,n)=1' means that user 'm_prn' can cancel the signal of user 'm' on resource block 'n'
%Determine user grouping and active users on each resource block (group)
if N==1
    fprintf('----------Single-Cluster: FD-NOMA is identical to SC-NOMA----------\n\n\n')
    rho=ones(K,N);
else
    %%NOMA clustering:
    [rho]=func_NOMA_Clustering(K,N,Decod_order); %rho(k,n)=1: User k belongs to group n or occupies resource block n
end
for n=1:N
    activeuser{n}=find(rho(:,n)); %Set of active users on each group
end
%%Modify the decoding order based on the adopted 'rho'
[Modify_order]=func_Decoding_order_Modify(K,N,rho,Decod_order); %Modify ordering numbers from 1 to oder of cluster, and set zero for users do not belong to the cluster set
for n=1:N
    cluster_head(n)=find(Modify_order(:,n)==max(Modify_order(:,n))); %Vector of Cluster-head users index
end

%%Power Minimization Problem (Feasibility problem)
[Q_min,p]=func_PowMin(K,N,W_s,R_min,h,Modify_order,activeuser); %Find lower bound Q^{min}_n as well as optimal powers
P_powmin_FDNOMA=sum(Q_min); %Minimal total power consumption to satisfy minimum rate demands (in Watts)
[r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
%%Feasibility Check
fprintf('***Power minimization problem***\n\n\n')
%%%%%Results
fprintf('Minimum rate demand of users (Mbps):\n')
disp(R_min/1e+6)
fprintf('Achievable rate of users (Mbps):\n')
rate_FDNOMA=W_s*r/1e+6;
disp(rate_FDNOMA)
fprintf('Power consumption of each user group (mW):\n')
disp(Q_min*1e+3)
fprintf('Total power consumption (mW):\n')
disp(P_powmin_FDNOMA*1e+3)
fprintf('Total power consumption in percentage of maximum cellular power:\n')
disp(100*P_powmin_FDNOMA/P_max)
if ( P_powmin_FDNOMA>P_max )
    Feasible_FDNOMA=0;
    fprintf('---Infeasible! (Not Enough Power!)---\n\n')
else Feasible_FDNOMA=1;
    fprintf('---Feasible!---\n\n')
end

%%Sum-Rate Maximization Problem
if Feasible_FDNOMA==1
    [q,alpha,c]=func_SumRateMax_q(K,N,W_s,R_min,h,activeuser,...
        Modify_order,cluster_head,P_max,P_mask,Q_min); %Intra-cluster power allocation
    [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
    [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
    fprintf('***Sum-rate maximization problem***\n\n\n')
    %%%%%Results
    fprintf('Minimum rate demand of users (Mbps):\n')
    disp(R_min/1e+6)
    fprintf('Achievable rate of users (Mbps):\n')
    rate_FDNOMA=W_s*r/1e+6;
    disp(rate_FDNOMA)
    fprintf('Cluster-head user`s index:\n')
    disp(cluster_head)
    fprintf('Each users group sum-rate (Mbps):\n')
    R_GP_FDNOMA=zeros(1,N);
    for n=1:N
        R_GP_FDNOMA(n)=sum(rho(:,n)'.*rate_FDNOMA);
    end
    disp(R_GP_FDNOMA)
    fprintf('Sum-rate of users (Mbps):\n')
    R_tot_FDNOMA=sum(rate_FDNOMA);
    disp(R_tot_FDNOMA)
    fprintf('NOMA clusters power budget in percentage of cellular power:\n')
    disp(100*q./P_max)
    fprintf('Total power consumption in percentage of maximum cellular power:\n')
    disp(100*sum(q)/P_max)
    
    %Equal inter-cluster power allocation:
    q=(P_max/N)*ones(1,N);
    [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
    [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
    fprintf('---Suboptimal: Equal inter-cluster power allocation---\n\n')
    %%%%%Results
    fprintf('---Each users group sum-rate (Mbps):\n')
    R_GP_FDNOMA_EPA=zeros(1,N);
    for n=1:N
        R_GP_FDNOMA_EPA(n)=sum(rho(:,n)'.*W_s.*r/1e+6);
    end
    disp(R_GP_FDNOMA_EPA)
    fprintf('---Sum-rate of all users (Mbps):\n')
    R_tot_FDNOMA_EPA=sum(R_GP_FDNOMA_EPA);
    disp(R_tot_FDNOMA_EPA)
    fprintf('---q in percentage\n')
    disp(100*q./P_max)
end

%%Energy Efficiency Maximization Problem
if Feasible_FDNOMA==1
    %Dinkelbach Algorithm - Lagrange dual with subgradient method
    [q,p,r,F,lambda_matrix,S,nu]=func_EEmax_Dinkelbach_Subgradient(K,N,W_s,R_min,h,activeuser,...
        Modify_order,cluster_head,P_max,P_mask,Q_min,eps_dinkel,P_C,cancel,eps_subgrad);
    fprintf('***Energy efficiency maximization problem: Subgradient Method***\n\n\n')
    %%%%%Results
    fprintf('Achievable rate of users (Mbps):\n')
    rate_FDNOMA=W_s*r/1e+6;
    disp(rate_FDNOMA)
    fprintf('Sum-rate of users (Mbps):\n')
    disp(sum(rate_FDNOMA))
    fprintf('System energy efficiency (Mbits/Joule):\n')
    EE_FDNOMA=sum(rate_FDNOMA)/(sum(q)+P_C);
    disp(EE_FDNOMA)
    fprintf('FDNOMA clusters power budget in percentage of cellular power:\n')
    disp(100*q./P_max)
    fprintf('Total power consumption in percentage of maximum cellular power:\n')
    disp(100*sum(q)/P_max)
    fprintf('Dinkelbach parameter lambda over iterations\n')
    disp(lambda_matrix)
    fprintf('Fractional function over Dinkelbach iterations: F(lambda,p)\n')
    disp(F)
    fprintf('F(lambda,p) over subgradient iterations at each Dinkelbach iteration\n')
    disp(S)
    
    %Dinkelbach Algorithm - Barrier algorithm with inner Newton's method
    [q,p,r,F,lambda_matrix,barrier_func]=func_EEmax_Dinkelbach_Barrier(K,N,W_s,R_min,h,activeuser,...
        Modify_order,cluster_head,P_max,P_mask,Q_min,eps_dinkel,P_C,cancel,...
        ALPHA,BETA,eps_N,epsilon_B,stepsize,tt,Max_Newton);
    fprintf('***Energy efficiency maximization problem: Barrier Method***\n\n\n')
    %%%%%Results
    fprintf('Achievable rate of users (Mbps):\n')
    rate_FDNOMA=W_s*r/1e+6;
    disp(rate_FDNOMA)
    fprintf('Sum-rate of users (Mbps):\n')
    disp(sum(rate_FDNOMA))
    fprintf('System energy efficiency (Mbits/Joule):\n')
    disp(sum(rate_FDNOMA)/(sum(q)+P_C))
    fprintf('NOMA clusters power budget in percentage of cellular power:\n')
    disp(100*q./P_max)
    fprintf('Total power consumption in percentage of maximum cellular power:\n')
    disp(100*sum(q)/P_max)
    fprintf('Dinkelbach parameter lambda over iterations\n')
    disp(lambda_matrix)
    fprintf('Fractional function over Dinkelbach iterations: F(lambda,p)\n')
    disp(F)
    fprintf('Barrier function in the last Dinkelback iteration\n')
    disp(barrier_func{numel(lambda_matrix)})
end

%% FDMA
fprintf('*********FDMA*********\n\n\n')
U_max=1; %no interference cancellation >> single user on each resource block
N=ceil(K/U_max); %number of subchannels (clusters)
W_s=W/N; %Bandwidth of each subchannel
N0=W_s*repmat(PSD_users',1,N); %AWGN power at users over subchannels
h=repmat(h_SC',1,N); %channel gains
h=h./N0; %channel gains normalized by noise
P_mask=P_max*ones(1,N); %No additional limit on the maximum power of the subchannels

%%Decoding Order: (We directly use the MATLAB functions of NOMA when U_max=1)
[Decod_order]=func_Decoding_order(K,N,h); %lower number corresponds to lower decoding order
[cancel]=func_Cancellation(K,N,Decod_order); %'Cancel(m,m_prn,n)=1' means that user 'm_prn' can cancel the signal of user 'm' on resource block 'n'
%subchannel allocation:
rho=eye(K); %each user occupies one resource block with equal length
for n=1:N
    activeuser{n}=find(rho(:,n)); %Set of active users on each group (each group consists of one user)
end
%%Modify the decoding order based on the adopted 'rho'
[Modify_order]=func_Decoding_order_Modify(K,N,rho,Decod_order); %Modify ordering numbers from 1 to oder of cluster, and set zero for users do not belong to the cluster set
for n=1:N
    cluster_head(n)=find(Modify_order(:,n)==max(Modify_order(:,n))); %Vector of Cluster-head users index
end

%%Power Minimization Problem (Feasibility problem)
[Q_min,p]=func_PowMin(K,N,W_s,R_min,h,Modify_order,activeuser); %Find lower bound Q^{min}_n as well as optimal powers
P_powmin_FDMA=sum(Q_min); % Minimal total power consumption to satisfy minimum rate demands (in Watts)
[r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
%%Feasibility Check
fprintf('***Power minimization problem***\n\n\n')
%%%%%Results
fprintf('Minimum rate demand of users (Mbps):\n')
disp(R_min/1e+6)
fprintf('Achievable rate of users (Mbps):\n')
rate_FDMA=W_s*r/1e+6;
disp(rate_FDMA)
fprintf('Total power consumption (mW):\n')
disp(P_powmin_FDMA*1e+3)
fprintf('Total power consumption in percentage of maximum cellular power:\n')
disp(100*P_powmin_FDMA/P_max)
if ( P_powmin_FDMA>P_max )
    Feasible_FDMA=0;
    fprintf('---Infeasible! (Not Enough Power!)---\n\n')
else Feasible_FDMA=1;
    fprintf('---Feasible!---\n\n')
end

%%Sum-Rate Maximization Problem
if Feasible_FDMA==1
    [q,alpha,c]=func_SumRateMax_q(K,N,W_s,R_min,h,activeuser,...
        Modify_order,cluster_head,P_max,P_mask,Q_min); %Intra-cluster power allocation
    [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
    [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
    fprintf('***Sum-rate maximization problem***\n\n\n')
    %%%%%Results
    fprintf('Minimum rate demand of users (Mbps):\n')
    disp(R_min/1e+6)
    fprintf('Achievable rate of users (Mbps):\n')
    rate_FDMA=W_s*r/1e+6;
    disp(rate_FDMA)
    fprintf('Sum-rate of users (Mbps):\n')
    R_tot_FDMA=sum(rate_FDMA);
    disp(R_tot_FDMA)
    fprintf('Total power consumption in percentage of maximum cellular power:\n')
    disp(100*sum(q)/P_max)
    
    %Equal inter-cluster power allocation:
    q=(P_max/N)*ones(1,N);
    [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
    [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
    fprintf('---Suboptimal: Equal inter-cluster power allocation---\n\n')
    %%%%%Results
    fprintf('---Sum-rate of all users (Mbps):\n')
    R_tot_FDMA_EPA=sum(W_s*r/1e+6);
    disp(R_tot_FDMA_EPA)
    fprintf('---q in percentage\n')
    disp(100*q./P_max)
end

%%Energy Efficiency Maximization Problem
if Feasible_FDMA==1
    %Dinkelbach Algorithm - Lagrange dual with subgradient method
    [q,p,r,F,lambda_matrix,S,nu]=func_EEmax_Dinkelbach_Subgradient(K,N,W_s,R_min,h,activeuser,...
        Modify_order,cluster_head,P_max,P_mask,Q_min,eps_dinkel,P_C,cancel,eps_subgrad);
    fprintf('***Energy efficiency maximization problem: Subgradient Method***\n\n\n')
    %%%%%Results
    fprintf('Achievable rate of users (Mbps):\n')
    rate_FDMA=W_s*r/1e+6;
    disp(rate_FDMA)
    fprintf('Sum-rate of users (Mbps):\n')
    disp(sum(rate_FDMA))
    fprintf('System energy efficiency (Mbits/Joule):\n')
    EE_FDMA=sum(rate_FDMA)/(sum(q)+P_C);
    disp(EE_FDMA)
    fprintf('Total power consumption in percentage of maximum cellular power:\n')
    disp(100*sum(q)/P_max)
    fprintf('Dinkelbach parameter lambda over iterations\n')
    disp(lambda_matrix)
    fprintf('Fractional function over Dinkelbach iterations: F(lambda,p)\n')
    disp(F)
    fprintf('F(lambda,p) over subgradient iterations at each Dinkelbach iteration\n')
    disp(S)
    
    %Dinkelbach Algorithm - Barrier algorithm with inner Newton's method
    [q,p,r,F,lambda_matrix,barrier_func]=func_EEmax_Dinkelbach_Barrier(K,N,W_s,R_min,h,activeuser,...
        Modify_order,cluster_head,P_max,P_mask,Q_min,eps_dinkel,P_C,cancel,...
        ALPHA,BETA,eps_N,epsilon_B,stepsize,tt,Max_Newton);
    fprintf('***Energy efficiency maximization problem: Barrier Method***\n\n\n')
    %%%%%Results
    fprintf('Achievable rate of users (Mbps):\n')
    rate_FDMA=W_s*r/1e+6;
    disp(rate_FDMA)
    fprintf('Sum-rate of users (Mbps):\n')
    disp(sum(rate_FDMA))
    fprintf('System energy efficiency (Mbits/Joule):\n')
    disp(sum(rate_FDMA)/(sum(q)+P_C))
    fprintf('Total power consumption in percentage of maximum cellular power:\n')
    disp(100*sum(q)/P_max)
    fprintf('Dinkelbach parameter lambda over iterations\n')
    disp(lambda_matrix)
    fprintf('Fractional function over Dinkelbach iterations: F(lambda,p)\n')
    disp(F)
    fprintf('Barrier function in the last Dinkelback iteration\n')
    disp(barrier_func{numel(lambda_matrix)})
end
