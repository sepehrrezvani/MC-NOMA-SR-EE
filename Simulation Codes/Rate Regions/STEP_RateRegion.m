%% Achievable Rate regions of FD-NOMA, and FDMA for two user groups over a flat fading channel
clear all
clc
%% Network Topology and General System Settings
BS_Type=1; %Macro-BS: (BS_Type=1) / Femto-BS: (BS_Type=0).
radius_BS=BS_Type*500+(1-BS_Type)*40; %radius of MBS/FBS in meter
K=17; %Number of users
U_NOMA=ceil(K/2); %Maximum number of multiplexed users in NOMA
min_Dis_User=BS_Type*20+(1-BS_Type)*2; %minimum distance of user to associated BS in meter
W=5e+6; %wireless bandwidth (Hz)
PSD_N0=-174; %PSD of Noise in dBm
PSD_N0=10.^((PSD_N0-30)/10); %PSD of Noise in watts
PSD_users=abs(randn(1,K)).*PSD_N0; %PSD of AWGN at users
Cor_user=func_CoordinateUser(K,radius_BS,min_Dis_User); %Coordinate of users (uniform distribution)
h=func_SingleChannelGain(K,Cor_user,BS_Type); %(flat fading) channel gains
h_SC=h;%save (single) channels
%%%%%Transmit power of BSs
P_max=BS_Type*46+(1-BS_Type)*30; %BS power in dbm
P_max=10.^((P_max-30)./10); %BS power in Watts
%%%%%Minimum rate demands
% R_min=2e+6.*abs(randn(1,K)); %bps
R_min=1.5e+6*ones(1,K); %bps

%Samples for power budgets
samples=9999; %set a value as '(10^n)-1'

%% Figure of Network Topology: Save as 'Fig_topology.fig'
[Leg_coverage_BS]=func_FigTopology(K,radius_BS,Cor_user);
% clear Dis_BS Cor_BS radius_BS min_Dis_User Cor_user Leg_coverage_BS b F_cell ...
%   M_cell R_macro R_femto

%% SC-NOMA: Multiplexing all users over a single flat fading channel
fprintf('*********SC-NOMA: Benchmark*********\n\n\n')
U_max=K; %all users are multiplexed
N=ceil(K/U_max); %number of clusters (here is 1), or number of resource blocks
W_s=W/N; %bandwidth of each subchannel
N0=W_s*repmat(PSD_users',1,N); %AWGN power at users over the single channel
h=repmat(h_SC',1,N); %channel gains
h=h./N0; %channel gains normalized by noise
P_mask=P_max*ones(1,N); %No additional limit on the maximum power of the single channel

%Determine user grouping and active users on each resource block (group)
rho=ones(K,N); 
for n=1:N
    activeuser{n}=find(rho(:,n)); %Set of active users on each group (or time block)
end

%%Decoding Order
[Decod_order]=func_Decoding_order(K,N,h); %lower number corresponds to lower decoding order
[cancel]=func_Cancellation(K,N,Decod_order); %'Cancel(m,m_prn,n)=1' means that user 'm_prn' can cancel the signal of user 'm' on resource block 'n'
[Modify_order]=func_Decoding_order_Modify(K,N,rho,Decod_order); %Modify ordering numbers from 1 to oder of cluster, and set zero for users do not belong to the cluster set
for n=1:N
    cluster_head(n)=find(Modify_order(:,n)==max(Modify_order(:,n))); %Vector of Cluster-head users index
end

%%Power Minimization Problem (Feasibility problem)
[Q_min,p]=func_PowMin(K,N,W_s,R_min,h,Modify_order,activeuser); %Find lower bound Q^{min}_n as well as optimal powers
P_powmin_FSCSIC=sum(Q_min); % Minimal total power consumption to satisfy minimum rate demands (in Watts)
[r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
%%Feasibility Check
if ( P_powmin_FSCSIC>P_max )
    Feasible_FSCSIC=0;
    fprintf('---Infeasible! (Not Enough Power!)---\n\n')
else Feasible_FSCSIC=1;
    fprintf('---Feasible!---\n\n')
end

%%Sum-Rate Maximization Problem
if Feasible_FSCSIC==1
    [q,alpha,c]=func_SumRateMax_q(K,N,W_s,R_min,h,activeuser,...
        Modify_order,cluster_head,P_max,P_mask,Q_min); %Intra-cluster power allocation
    [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
    [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
    %%%%%Results
    rate_FSCSIC=W_s*r/1e+6;
    fprintf('Sum-rate of users (Mbps):\n')
    R_tot_FSCSIC=sum(rate_FSCSIC);
    disp(R_tot_FSCSIC)
end

%% FDMA-NOMA (FD-NOMA): User grouping over subchannels (flat fading channels)
fprintf('*********FDMA-NOMA (FD-NOMA)*********\n\n\n')
U_max=U_NOMA; %maximum number of multiplexed users
N=ceil(K/U_max); %number of subchannels (clusters)
W_s=W/N; %Bandwidth of each subchannel
N0=W_s*repmat(PSD_users',1,N); %AWGN power at users over subchannels
h=repmat(h_SC',1,N); %channel gains
h=h./N0; %channel gains normalized by noise
P_mask=P_max*ones(1,N); %No additional limit on the maximum power of the subchannels

%%Decoding Order
[Decod_order]=func_Decoding_order(K,N,h); %lower number corresponds to lower decoding order
[cancel]=func_Cancellation(K,N,Decod_order); %'Cancel(m,m_prn,n)=1' means that user 'm_prn' can cancel the signal of user 'm' on resource block 'n'
%Determine user grouping and active users on each resource block (group)
[rho]=func_NOMA_Clustering(K,N,Decod_order); %rho(k,n)=1: User k belongs to group n or occupies resource block n
rho_NOMA=rho;%save NOMA groups
for n=1:N
    activeuser{n}=find(rho(:,n)); %Set of active users on each group
end
%%Modify the decoding order based on the adopted 'rho'
[Modify_order]=func_Decoding_order_Modify(K,N,rho,Decod_order); %Modify ordering numbers from 1 to oder of cluster, and set zero for users do not belong to the cluster set
for n=1:N
    cluster_head(n)=find(Modify_order(:,n)==max(Modify_order(:,n))); %Vector of Cluster-head users index
end
cluster_head_NOMA=cluster_head;

%%Power Minimization Problem (Feasibility problem)
[Q_min,p]=func_PowMin(K,N,W_s,R_min,h,Modify_order,activeuser); %Find lower bound Q^{min}_n as well as optimal powers
P_powmin_FDNOMA=sum(Q_min); %Minimal total power consumption to satisfy minimum rate demands (in Watts)
[r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
%%Feasibility Check
if ( P_powmin_FDNOMA>P_max )
    Feasible_FDNOMA=0;
    fprintf('---Infeasible! (Not Enough Power!)---\n\n')
else Feasible_FDNOMA=1;
    fprintf('---Feasible!---\n\n')
end

%%Achievable rate region if the feasible region is nonempty
if Feasible_FDNOMA==1
    q_s=P_max/samples;%stepsize for increasing power budget of clusters
    budget_vec=[P_max:-q_s:0]; %power budget of cluster 1
    RR_FDNOMA=zeros(N,numel(budget_vec));
    for ss=1:numel(budget_vec) %from maximum power budget for cluster 1 to zero power budget for cluster 1
        q=[budget_vec(ss),P_max-budget_vec(ss)]; %clusters power budget vector (W)
        [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
        [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
        rate_FDNOMA=W_s*r/1e+6; %rate in Mbps
        R_GP_FDNOMA=zeros(1,N);
        for n=1:N
            R_GP_FDNOMA(n)=sum(rho(:,n)'.*rate_FDNOMA);
        end
        RR_FDNOMA(:,ss)=R_GP_FDNOMA;
    end
end

%% FDMA
fprintf('*********FDMA*********\n\n\n')
U_max=1; %no interference cancellation >> single user on each resource block
N=ceil(K/U_max); %number of subchannels (clusters)
W_s=W/N; %Bandwidth of each subchannel
N0=W_s*repmat(PSD_users',1,N); %AWGN power at users over subchannels
h=repmat(h_SC',1,N); %channel gains
h=h./N0; %channel gains normalized by noise
P_mask=P_max*ones(1,N); %No additional limit on the maximum power of the subchannels

%%Decoding Order: (We directly use the MATLAB functions of NOMA when U_max=1)
[Decod_order]=func_Decoding_order(K,N,h); %lower number corresponds to lower decoding order
[cancel]=func_Cancellation(K,N,Decod_order); %'Cancel(m,m_prn,n)=1' means that user 'm_prn' can cancel the signal of user 'm' on resource block 'n'
%subchannel allocation:
rho=eye(K); %each user occupies one resource block with equal length
for n=1:N
    activeuser{n}=find(rho(:,n)); %Set of active users on each group (each group consists of one user)
end
%%Modify the decoding order based on the adopted 'rho'
[Modify_order]=func_Decoding_order_Modify(K,N,rho,Decod_order); %Modify ordering numbers from 1 to oder of cluster, and set zero for users do not belong to the cluster set
for n=1:N
    cluster_head(n)=find(Modify_order(:,n)==max(Modify_order(:,n))); %Vector of Cluster-head users index
end

%%Power Minimization Problem (Feasibility problem)
[Q_min,p]=func_PowMin(K,N,W_s,R_min,h,Modify_order,activeuser); %Find lower bound Q^{min}_n as well as optimal powers
P_powmin_FDMA=sum(Q_min); % Minimal total power consumption to satisfy minimum rate demands (in Watts)
[r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
%%Feasibility Check
if ( P_powmin_FDMA>P_max )
    Feasible_FDMA=0;
    fprintf('---Infeasible! (Not Enough Power!)---\n\n')
else Feasible_FDMA=1;
    fprintf('---Feasible!---\n\n')
end

%%Minimum power budget of each NOMA group in TDMA
for cls=1:2
    Q_min_cluster(cls)=sum(rho_NOMA(:,cls).*Q_min');
end
%%Achievable Rate if the feasible region is nonempty
if Feasible_FDMA==1
    RR_FDMA=zeros(2,numel(budget_vec));
    for ss=1:numel(budget_vec) %from maximum power budget for cluster 1 to zero power budget for cluster
        q=[budget_vec(ss),P_max-budget_vec(ss)]; %clusters power budget vector (W)
        p=zeros(K,N); %Intra-cluster power allocation: Finding optimal p
        P_FDMA=[];
        for cls=1:2
            if q(cls)>=Q_min_cluster(cls)
                [p_cls]=func_SumRateMax_p_FDMA(K,cls,h,rho_NOMA,cluster_head_NOMA,Q_min,q);
                P_FDMA{cls}=rho.*repmat(p_cls,1,N);
            else P_FDMA{cls}=zeros(K,N);
            end
        end
        p=P_FDMA{1}+P_FDMA{2};%optimal powers among users for the given power budget
        r=sum(log2(1+p.*h),2)'; %achievable rate of users in FDMA based on the given power budget
        R_GP_FDMA=zeros(1,2); %achievable rate (Mbps) of each user group in each sample 'ss'
        for cls=1:2
            R_GP_FDMA(cls)=W_s*sum(rho_NOMA(:,cls)'.*r)/1e+6;
        end
        RR_FDMA(:,ss)=R_GP_FDMA;
    end
end

%% Plot Figure: Rate Region
Fig_rateregion=figure;
hold on
plot(RR_FDNOMA(1,:),RR_FDNOMA(2,:),'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',5,'Marker','o','linewidth',1.5,'Color','k');
plot(RR_FDMA(1,:),RR_FDMA(2,:),'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',5,'Marker','o','linewidth',1.5,'Color','k','linestyle','--');

legend('FD-NOMA','FDMA','location','southwest');
set(legend,'FontSize',12,'FontName','Times New Roman','EdgeColor',[0 0 0])
xlabel('R_1 (Mbps)','FontSize',16,'FontName','Times New Roman')
ylabel('R_2 (Mbps)','FontSize',16,'FontName','Times New Roman')
% set(gca,'XTick',num_Fcell_user,'YScale', 'log','FontName','Times New Roman','FontSize',14)
set(gca,'FontSize',12)
grid on
savefig(Fig_rateregion,'Fig_rateregion')

%% Plot Figure: Sum-rate Regions for different power budgets
Fig_sumrate_region=figure;
hold on

plot(P_max-budget_vec,R_tot_FSCSIC*ones(1,numel(budget_vec)),'linewidth',1.5,'Color','r');
plot(P_max-budget_vec,sum(RR_FDNOMA,1),'linewidth',1.5,'Color','k');
plot(P_max-budget_vec,sum(RR_FDMA,1),'linewidth',1.5,'Color','k','linestyle','--');

legend('SC-NOMA','FD-NOMA','FDMA','location','south');
set(legend,'FontSize',12,'FontName','Times New Roman','EdgeColor',[0 0 0])
xlabel('Power budget of group 2 (W)','FontSize',16,'FontName','Times New Roman')
ylabel('Sum-rate of all users (Mbps)','FontSize',16,'FontName','Times New Roman')
% set(gca,'XTick',num_Fcell_user,'YScale', 'log','FontName','Times New Roman','FontSize',14)
set(gca,'FontSize',12)
grid on
savefig(Fig_sumrate_region,'Fig_sumrate_region')
